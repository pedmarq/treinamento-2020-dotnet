﻿using System;


namespace basico
{
    public class Cat
    {
        public DateTime dataNascimento;
        public string Nome;

        public Cat()
        {
            this.dataNascimento = DateTime.Now;
        }

        public Cat(string nome) : this()
        {
            this.Nome = nome;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello turminha animada da noite!");

            var nome = "dotnet .....";

            Console.WriteLine(nome);

            nome = "Alex Gamas";

            Console.WriteLine(nome);

            String texto = $"O dado contido {{ na variável nome é {nome}";

            Console.WriteLine($"este é o texto '{texto}'");

            string meuNome = "Alex";

            int idade = 36;

            // double altura = 1.73;

            float altura = 1.73f;

            Console.WriteLine("Nome:   {0}", meuNome);
            Console.WriteLine("Idade:  {0}", idade);
            Console.WriteLine("Altura: {0}", altura);

            var numero = 0.003;

            Console.WriteLine(numero.GetType());
            Console.WriteLine(typeof(double));


            Console.WriteLine(numero.GetType() == typeof(double));

            Console.WriteLine(numero is double);

            bool IsDouble(object o)
            {
                return o.GetType() == typeof(double);
            }

            Console.WriteLine($" 12 é double? {IsDouble(12)}");
            Console.WriteLine($" 12.22 é double? {IsDouble(12.22)}");
            Console.WriteLine($" 12.22f é double? {IsDouble(12.22f)}");

            /**********************************************************************************************/
            // Precision

            // isso nao funciona
            // var nome2 = "vitor";
            // nome2 = 33;

            // float - 32 bit (7 digits)
            // double - 64 bit (15-16 digits)
            // decimal - 128 bit (28-29 significant digits)

            Console.WriteLine("--------------------------------------------");
            int distanciaInt = 13 / 7;
            float distanciaFloat = 13.0f / 7;
            double distanciaDouble = 13.0 / 7;
            decimal distanciaDecimal = 13.0m / 7;

            Console.WriteLine("distanciaInt {0}", distanciaInt);
            Console.WriteLine("distanciaFloat {0}", distanciaFloat);
            Console.WriteLine("distanciaDouble {0}", distanciaDouble);
            Console.WriteLine("distanciaDecimal {0}", distanciaDecimal);

            /**********************************************************************************************/
            // Type Cast

            Console.WriteLine("(int) distanciaDecimal {0}", (int)distanciaDecimal);
            Console.WriteLine("(float) distanciaDecimal {0}", (float)distanciaDecimal);
            Console.WriteLine("(double) distanciaDecimal {0}", (double)distanciaDecimal);

            decimal meuDecimalforcado = (decimal)((double)distanciaDecimal);

            Console.WriteLine("(decimal???) meuDecimalforcado {0}", (double)distanciaDecimal);

            // // Não adianta tentar aumentar a precisao na formataçao
            Console.WriteLine("distanciaInt {0:F4}", distanciaInt);


            /**********************************************************************************************/
            // Null

            // int? numeroQuePodeSerNulo = null;
            Nullable<int> numeroQuePodeSerNulo = null;

            Console.WriteLine($"Numero Tem Valor? {numeroQuePodeSerNulo.HasValue}");
            Console.WriteLine($"Numero: [{numeroQuePodeSerNulo}]");

            if (numeroQuePodeSerNulo.HasValue)
            {
                Console.WriteLine($"Numero Tem Valor");
            }
            else
            {
                Console.WriteLine($"Numero NAO Tem Valor");
            }

            numeroQuePodeSerNulo = 3;


            if (numeroQuePodeSerNulo.HasValue)
            {
                Console.WriteLine($"Numero Tem Valor e é {numeroQuePodeSerNulo.Value}");
            }
            else
            {
                Console.WriteLine($"Numero NAO Tem Valor");
            }

            Console.WriteLine($"Numero  Tem Valor? {numeroQuePodeSerNulo.HasValue}");
            Console.WriteLine($"Numero: {numeroQuePodeSerNulo}");
            Console.WriteLine($"Numero: {numeroQuePodeSerNulo.Value}");


            Cat gato = null;

            // gato = new Cat();
            // gato.Nome = "possivel garfield";

            // var garfield = gato != null ? gato : new Cat();

            // ?? é um nulish coalescense operator
            var garfield = gato ?? new Cat();

            if (gato != null)
            {
                Console.WriteLine($"gato nao é nulo");
                Console.WriteLine($"Valor: {garfield}");
            }
            else
            {
                Console.WriteLine($"gato é nulo");
            }
        }
    }
}
